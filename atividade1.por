programa
{
	
	funcao inicio()
	{
		real n1, n2, n3, media

		escreva("digite a nota 1: ")
		leia(n1)
		
		escreva("digite a nota 2: ")
		leia(n2)
		
		escreva("digite a nota 3: ")
		leia(n3)

		media = (n1 + n2 + n3)/3
		
		se(media>=6){
			
		escreva("aprovado: ", media)
		
		}senao{
			escreva("reprovado: ", media)
			}
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 221; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */