programa
{
	
	funcao inicio()
	{
		real valorReal, valorDolar, cotacao

		
		escreva("digite a cotacao: ")
		leia(cotacao)

		escreva("digite valorDolar: ")
		leia(valorDolar)

		valorReal = valorDolar * cotacao
				
		escreva("valor real: ", valorReal)
		 	
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 274; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */