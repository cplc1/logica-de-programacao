programa
{
	
	funcao inicio()
	{
		real tempo, velocidade, distancia, litros_usados
		escreva("digite o tempo: ")
		leia (tempo)
		
		escreva("digite a velocidade: ")
		leia(velocidade)

		distancia = tempo * velocidade
		litros_usados = distancia / 12

		escreva("valor tempo: ", tempo)
		escreva("\n") //quebra de linha
		escreva("valor velocidade: ", velocidade)
		escreva("\n")
		escreva("valor distancia:", distancia)
		escreva("\n")
		escreva("valor litros usados:", litros_usados)
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 73; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */