programa
{
	
	funcao inicio()
	{
		real velocidade, distancia, tempo

		escreva("distancia: ")
    		leia(distancia)
   		escreva("tempo: ")
    		leia(tempo)

    		velocidade = (distancia * 1000)/(tempo * 60)
    		
    		escreva("velocidade: ", velocidade, " m/s")
    		leia(velocidade)

    	
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 302; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */