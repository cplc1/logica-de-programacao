programa
{
	
	funcao inicio()
	{
		inteiro num1, num2

		escreva("Digite o primeiro valor:")
		leia(num1)
		
		escreva("Digite o segundo valor:")
		leia(num2)

		se(num1 < num2){
		escreva("O menor numero é: ", num1, " O maior numero é: ", num2)

		}senao se(num1 == num2){
			escreva("Os numeros são iguais.")
			
			}senao{
				escreva("O menor numero é: ", num2, " O maior numero é: ", num1)
				}
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 405; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */